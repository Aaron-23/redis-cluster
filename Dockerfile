FROM redis:5.0.5

ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
ENV REDIS_C=/etc/redis
RUN echo "deb http://mirrors.aliyun.com/debian  stable main contrib non-free" > /etc/apt/sources.list && \
echo "deb http://mirrors.aliyun.com/debian  stable-updates main contrib non-free" >> /etc/apt/sources.list && \
apt-get update && \
apt-get -y install net-tools iputils-ping telnet dnsutils &&  \
rm -rf /var/lib/apt/lists/* && \ 
apt-get clean 

ADD redis.conf $REDIS_C/redis.conf
ADD start.sh $REDIS_C/start.sh

VOLUME ["/var/log/redis/"]
VOLUME ["/var/lib/redis"]
EXPOSE 6379
CMD [ "redis-server", "/etc/redis/redis.conf" ]

#CMD ["/start.sh"]